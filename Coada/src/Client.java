import java.util.Random;

public class Client {
	private int arrivalTime;
	private int processingTime;
	private int finishTime;
	private int id;
	
	public Client(int arrivalTime,int processingTime){
		this.arrivalTime=arrivalTime;
		this.processingTime=processingTime;
	}
	
	public int getArrivalTime() {
		return arrivalTime;
	}
	public int getProcessingTime() {
		return processingTime;
	}
	
	public void setFinishTime(int wait) {
		finishTime=arrivalTime+processingTime+wait;
	}
	
	public int getFinishTime() {
		return finishTime;
	}

      public int getId() {
		return id;
	}
      public void setId(int id) {
		this.id = id;
	}


}
