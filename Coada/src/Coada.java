import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Coada extends Thread {
	private BlockingQueue<Client> clients;
	private AtomicInteger waitingPeriod=new AtomicInteger(0);
	private int id;
	public String log="";
	
	
	public Coada(){
		clients=new LinkedBlockingQueue<Client>();
	}
	
	
	
	public void addClient(Client c) throws InterruptedException {
		waitingPeriod.addAndGet(c.getProcessingTime());	
		clients.put(c);
	}
	
	public void deleteClient(Client c){
		clients.remove(c);
	}

	public int getWaitingPeriod() {
		return waitingPeriod.get();
	}

	public void setWaitingPeriod(int time2) {
		waitingPeriod.set(time2); 
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getContentCoada() {
		String contentCoada="";
		
		for(Client c:clients){
			
			contentCoada+=" Client[ "+c.getArrivalTime()+"][ "+c.getProcessingTime()+"]; "; 
		}
		
		return contentCoada;
	}
	
	
	
	
	public int getNoOfClients(){
		int nrClients=0;
		for(Client c:clients){
			nrClients++;
		}
		return nrClients;
	}


	

	@Override
	public void run() {
		
			try{
				while(true)
				{
					String log1;
					Client c;
					if(!clients.isEmpty()){
					c = clients.remove();
					Thread.sleep(c.getProcessingTime()*1000);	
				//	System.out.println("Client " +c.getId()+" scos din coada"+ this.getId());
					log1="Client " +c.getId()+" scos din coada"+ this.getId();
					setWaitingPeriod(getWaitingPeriod()-c.getProcessingTime());
					SimulationManage.frame.logArea.setText(SimulationManage.frame.logArea.getText()+log1+"\n");
					
			} 
				}
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		
	
	
	

}
