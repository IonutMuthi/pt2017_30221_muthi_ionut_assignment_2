import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimulationFrame extends JFrame{
	private static final long serialVersionUID=1L;
	private JPanel panel;
	private int width=600,height=600;
	private int nrClients;
	private int nrQueue;
	private int maxNrClients;
	private int minProcessingTime;
	private int maxProcessingTime;
	public int simulationTime;
	public static int time=0;
	public SelectionPolicy selectionPolicy=SelectionPolicy.SHORTEST_TIME;
	public static JTextArea logArea=new JTextArea("\n");;
	public static JTextArea coadaArea=new JTextArea("\n");
	public static SimulationManage manager;
	public ArrayList<String> outputID=new ArrayList(nrQueue);
	public static JLabel lbl7=new JLabel();
	private JTextField txt1=new JTextField();
	private JTextField txt2=new JTextField();
	private JTextField txt3=new JTextField();
	private JTextField txt4=new JTextField();
	private JTextField txt5=new JTextField();
	private JTextField txt6=new JTextField();
	
	
	public SimulationFrame(){
		panel=new JPanel();
		panel.setLayout(null);
		this.setSize(width,height);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
	
		JLabel lbl1=new JLabel();
		lbl1.setText("Max number of clients per server");
		lbl1.setBounds(10, 50, 200, 20);
		lbl1.setVisible(true);
		panel.add(lbl1);
		
		
		txt1.setBounds(200, 50, 80, 20);
		txt1.setEnabled(true);
		txt1.setVisible(true);
		panel.add(txt1);		
		this.add(panel);
		this.setVisible(true);
		
		JLabel lbl2=new JLabel();
		lbl2.setText("Max number of servers");
		lbl2.setBounds(10, 75, 200, 20);
		lbl2.setVisible(true);
		panel.add(lbl2);
		
		
		txt2.setBounds(200, 75, 80, 20);
		txt2.setEnabled(true);
		txt2.setVisible(true);	
		panel.add(txt2);		
		this.add(panel);
		this.setVisible(true);
		
		JLabel lbl3=new JLabel();
		lbl3.setText("Min processing time");
		lbl3.setBounds(10, 100, 200, 20);
		lbl3.setVisible(true);
		panel.add(lbl3);
		
	
		txt3.setBounds(200, 100, 80, 20);
		txt3.setEnabled(true);
		txt3.setVisible(true);		
		panel.add(txt3);		
		this.add(panel);
		this.setVisible(true);
		
		JLabel lbl4=new JLabel();
		lbl4.setText("Max processing time");
		lbl4.setBounds(10, 125, 200, 20);
		lbl4.setVisible(true);
		panel.add(lbl4);
		
		
		txt4.setBounds(200, 125, 80, 20);
		txt4.setEnabled(true);
		txt4.setVisible(true);		
		panel.add(txt4);		
		this.add(panel);
		this.setVisible(true);

		JLabel lbl5=new JLabel();
		lbl5.setText("Simulation time");
		lbl5.setBounds(10, 150, 200, 20);
		lbl5.setVisible(true);
		panel.add(lbl5);
		
		
		txt5.setBounds(200, 150, 80, 20);
		txt5.setEnabled(true);
		txt5.setVisible(true);
		panel.add(txt5);		
		
		JLabel lbl6=new JLabel();
		lbl6.setText("Number of clients");
		lbl6.setBounds(10, 175, 200, 20);
		lbl6.setVisible(true);
		panel.add(lbl6);
		
		
		txt6.setBounds(200, 175, 80, 20);
		txt6.setEnabled(true);
		txt6.setVisible(true);
		panel.add(txt6);		
		
		
		lbl7.setText(Integer.toString(time));
		lbl7.setBounds(10, 20, 100, 20);
		lbl7.setVisible(true);
		panel.add(lbl7);
		
		

		JButton btn2=new JButton();
		btn2.setBounds(30, 210, 100, 25);
		btn2.setText("Time sort");
		btn2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent argv0) {
				
			
			try {
				setSelectionPolicy(SelectionPolicy.SHORTEST_TIME);	
				  manager = new SimulationManage();			
				  manager.setMaxClientsPerServer(maxNrClients=Integer.parseInt(txt1.getText()));			
				  manager.setMaxNoServers(nrQueue=Integer.parseInt(txt2.getText()));			
				  manager.setMinProcessingTime(minProcessingTime=Integer.parseInt(txt3.getText()));			
				  manager.setMaxProcessingTime(maxProcessingTime=Integer.parseInt(txt4.getText()));			
				  manager.setTimeLimit(simulationTime=Integer.parseInt(txt5.getText()));			
				  manager.setNumberOfClients(nrClients=Integer.parseInt(txt6.getText()));			
				  logArea.setVisible(true);			
				  logArea.setText("Logger: \n");
				  
				  Thread mT=new Thread(manager);
				  mT.start();
				  
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}

		});
		
		btn2.setVisible(true);
		panel.add(btn2);
		
		JButton btn3=new JButton();
		btn3.setBounds(135, 210, 120, 25);
		btn3.setText("Number sort");
		btn3.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent argv0) {
				
			
			try {
				setSelectionPolicy(SelectionPolicy.SHORTEST_QUEUE);	
				manager = new SimulationManage();			
				  manager.setMaxClientsPerServer(maxNrClients=Integer.parseInt(txt1.getText()));			
				  manager.setMaxNoServers(nrQueue=Integer.parseInt(txt2.getText()));			
				  manager.setMinProcessingTime(minProcessingTime=Integer.parseInt(txt3.getText()));			
				  manager.setMaxProcessingTime(maxProcessingTime=Integer.parseInt(txt4.getText()));			
				  manager.setTimeLimit(simulationTime=Integer.parseInt(txt5.getText()));			
				  manager.setNumberOfClients(nrClients=Integer.parseInt(txt6.getText()));			
				  logArea.setVisible(true);			
				  logArea.setText("Logger: \n");
				  
				  Thread mT=new Thread(manager);
				  mT.start();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}

		});
		btn3.setVisible(true);
		panel.add(btn3);
		
		
		JScrollPane scrollPane=new JScrollPane();	
		scrollPane.setBounds(20, 320, 550, 200);
		scrollPane.setViewportView(logArea);
		getLogArea().setVisible(true);
		panel.add(scrollPane);
		
		JScrollPane scrollPane1=new JScrollPane();	
		scrollPane1.setBounds(300, 15, 270, 300);
		scrollPane1.setViewportView(coadaArea);
		getLogArea().setVisible(true);
		panel.add(scrollPane1);
		
	
		
		
		
		
		this.add(panel);
		this.setVisible(true);
		
	}
	
	


	public int getSimulationTime() {
		return simulationTime;
	}
	
	public int getNrQueue() {
		return nrQueue;
	}
	public int getMinProcessingTime() {
		return minProcessingTime;
	}
	public int getMaxProcessingTime() {
		return maxProcessingTime;
	}
	public int getNrClients() {
		return nrClients;
	}
	public int getMaxNrClients() {
		return maxNrClients;
	}
	
	
	public SelectionPolicy getSelectionPolicy() {
		return selectionPolicy;
	}
	
	public void setSelectionPolicy(SelectionPolicy selectionPolicy) {
		this.selectionPolicy = selectionPolicy;
	}
	
	
	
	public static void setLogArea(JTextArea logArea) {
		SimulationFrame.logArea = logArea;
	}
	
	public static JTextArea getLogArea() {
		return logArea;
	}
	
//	public static void main(String[] args) throws InterruptedException{
//		SimulationFrame simulation= new SimulationFrame();
//}
//		
	
}



