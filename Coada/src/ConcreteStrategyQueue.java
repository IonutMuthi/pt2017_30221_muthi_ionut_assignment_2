import java.util.ArrayList;

public class ConcreteStrategyQueue  implements Strategy{

	@Override
	public void addTask(ArrayList<Coada> servers, Client c) throws InterruptedException {
		int nrClients=Integer.MAX_VALUE;
		Coada serv=new Coada();
		for(Coada serv1: servers){
			if(serv1.getNoOfClients()<nrClients){
				nrClients=serv1.getNoOfClients();
			serv=serv1;
			}
		}
		c.setFinishTime(serv.getWaitingPeriod());
		serv.addClient(c);
		String log;
		log="client adaugat in coada " +serv.getId();
		SimulationManage.frame.logArea.setText(SimulationManage.frame.logArea.getText()+log+"\n");
		
	}

}
