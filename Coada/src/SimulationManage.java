

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JTextArea;

public class SimulationManage implements Runnable {
public int timeLimit;
public int maxProcessingTime;
public int minProcessingTime;
public int maxNoServers;
public int maxClientsPerServer;
public int numberOfClients;
public int time;
public SelectionPolicy selectionPolicy=SelectionPolicy.SHORTEST_TIME;
private Scheduler scheduler;
public JTextArea log;
public static SimulationFrame frame;
//private ArrayList<Client> generatedClients=new ArrayList<Client>();
private BlockingQueue<Client> generatedClient=new LinkedBlockingQueue();
	
	public SimulationManage() throws InterruptedException{
	
		
	}
	
	
	
	@Override
	public void run() {
	
		try {
			scheduler=new Scheduler(maxNoServers, maxClientsPerServer);
			ClientGenerator();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int curentTime=0;
		
		while(curentTime<timeLimit){
			
			try{
			for(Client c:generatedClient){
				
			if(c.getArrivalTime()==curentTime){
			scheduler.DispatchClient(c);
			//frame.logArea.setText(frame.logArea.getText()+ scheduler.getLogS());
			 SimulationManage.frame.coadaArea.setText(scheduler.getContentCoada());
			generatedClient.remove(c);
			
			
			Thread.sleep(2000);
			}
		}
	
			frame.lbl7.setText("Curent time: " + curentTime);
			curentTime++;
	}catch(InterruptedException ex){
		Thread.currentThread().interrupt();
	}
			}
		
	}
	

	
	
	
	public void ClientGenerator() throws InterruptedException{
		

		try{
			Random rand=new Random();
			for(int i=0;i<numberOfClients;i++){
			int arr,sta;
			sta=rand.nextInt(maxProcessingTime-minProcessingTime)+minProcessingTime;
			arr=rand.nextInt(timeLimit);
			Client c= new Client(arr,sta);
			c.setId(i);
			 generatedClient.put(c);
			}
			
		
			
	}catch(InterruptedException ex){
		Thread.currentThread().interrupt();
	}
			
		
	}
	
	public static void main(String[] args){
		frame=new SimulationFrame();
	}
	
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	
	public JTextArea getLog() {
		return log;
	}
	
	public void setLog(JTextArea log) {
		this.log = log;
	}
	
	public int getTimeLimit() {
		return timeLimit;
	}
	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}
	
	
	public int getMaxClientsPerServer() {
		return maxClientsPerServer;
	}
	
	public void setMaxClientsPerServer(int maxClientsPerServer) {
		this.maxClientsPerServer = maxClientsPerServer;
	}
	
	public int getNumberOfClients() {
		return numberOfClients;
	}
	
	public void setNumberOfClients(int numberOfClients) {
		this.numberOfClients = numberOfClients;
	}
	
	public int getMaxNoServers() {
		return maxNoServers;
	}
	
	public void setMaxNoServers(int maxNoServers) {
		this.maxNoServers = maxNoServers;
	}
	

	
	public int getMinProcessingTime() {
		return minProcessingTime;
	}
	
	public void setMinProcessingTime(int minProcessingTime) {
		this.minProcessingTime = minProcessingTime;
	}
	
	public int getMaxProcessingTime() {
		return maxProcessingTime;
	}
	
	public void setMaxProcessingTime(int maxProcessingTime) {
		this.maxProcessingTime = maxProcessingTime;
	}
	
	
}

