import java.awt.List;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Scheduler {
	private static ArrayList<Coada> servers=new ArrayList();
	private Strategy srategy=new ConcreteStrategyTime();
	public ArrayList<Thread> threads= new ArrayList<Thread>();
//	public static String contentCoada;

	

	public Scheduler(int maxNoServers,int  maxTasksPerServer) throws InterruptedException{
		for(int i=0;i<maxNoServers;i++){
			Coada coada= new Coada();
			coada.setId(i);
			servers.add(coada);
			Thread t = new Thread(coada);
			threads.add(t);
			t.start();
			
			
		}
	}
	
	public void changeStrategy(SelectionPolicy policy){
	
		if(policy==SelectionPolicy.SHORTEST_TIME){
			srategy=new ConcreteStrategyTime();
		}
		if(policy==SelectionPolicy.SHORTEST_QUEUE){
			srategy=new ConcreteStrategyQueue();
		}
	}
	

	public  String getContentCoada(){
		String contentCoada="";
		int repetitii;
		for(Coada c: servers){
			contentCoada+="Coada "+c.getId()+": "+c.getContentCoada()+" \n";
//				repetitii=c.getNoOfClients();
//				for(int i=0;i<repetitii;i++){
//					contentCoada+= " ^_^ ; "; 
//				}
//				contentCoada+="\n";
		}
		
		return contentCoada;
	}
	
	
	
	
	public void DispatchClient(Client c) throws InterruptedException{
		srategy.addTask(servers, c);
	}
	
	public ArrayList<Coada> getCozi(){
		return servers;
	}
	
}
